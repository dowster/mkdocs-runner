FROM python:3.9-alpine

# Install any mkdocs plugins
RUN python3 -m pip install \
    mkdocs \
    mkdocs-render-swagger-plugin \
    mkdocs-git-revision-date-plugin \
    mkdocs-material
